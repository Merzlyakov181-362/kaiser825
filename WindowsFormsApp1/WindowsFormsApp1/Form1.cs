﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        bool xTurn = true;
        public Form1()
        {
            InitializeComponent();
        }

        
        private void Button_Click(object sender, EventArgs e)
        {
            Button senderB = (Button)sender;
            if (xTurn)
            {
                senderB.Text = "X";
            }
            else
            {
                senderB.Text = "0";
            }
            xTurn = !xTurn;
            senderB.Enabled = false;

            CheckWin(senderB);
        }

        void CheckWin(Button pressedButton )
        {
            if(button1.Text == button2.Text && button2.Text == button3.Text && button2.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button4.Text == button5.Text && button5.Text == button6.Text && button5.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button7.Text == button8.Text && button8.Text == button9.Text && button9.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button1.Text == button4.Text && button4.Text == button7.Text && button1.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button2.Text == button5.Text && button5.Text == button8.Text && button8.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button3.Text == button6.Text && button6.Text == button9.Text && button9.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button1.Text == button5.Text && button5.Text == button9.Text && button5.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }

            if (button1.Text == button5.Text && button5.Text == button7.Text && button5.Enabled == false)
            {
                MessageBox.Show("Победа " + pressedButton.Text);
                Close();
            }


            

        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xTurn = true;

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            xTurn = false;
        }
    }
}
