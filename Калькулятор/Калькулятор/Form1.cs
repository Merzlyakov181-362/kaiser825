﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        bool pl = false;
        bool min = false;
        bool mn = false;
        bool del = false;
        bool st = false;
        bool sq = false;

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "7";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "1";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "5";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "9";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            pl = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";

        }

        private void button12_Click(object sender, EventArgs e)
        {
            min = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            mn = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            del = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            st = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            sq = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
 
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (pl)
            {
                Double pls = Convert.ToDouble(textBox1.Text) + Convert.ToDouble(textBox1.Tag);
                textBox1.Text = Convert.ToString(pls);
                pl = false;
            }

            if (min)
            {
                Double pls = Convert.ToDouble(textBox1.Tag) - Convert.ToDouble(textBox1.Text);
                textBox1.Text = Convert.ToString(pls);
                min = false;

            }

            if (mn)
            {
                Double pls = Convert.ToDouble(textBox1.Tag) * Convert.ToDouble(textBox1.Text);
                textBox1.Text = Convert.ToString(pls);
                mn = false;
            }

            if (del)
            {
                Double pls = Convert.ToDouble(textBox1.Tag) / Convert.ToDouble(textBox1.Text);
                textBox1.Text = Convert.ToString(pls);
                del = false;
            }

            if (st)
            {
                Double pls = Math.Pow(Convert.ToDouble(textBox1.Tag),Convert.ToDouble(textBox1.Text));
                textBox1.Text = Convert.ToString(pls);
                st = false;
            }

            if (sq)
            {
                Double pls = Math.Pow(Convert.ToDouble(textBox1.Text), 1.0 / Convert.ToDouble(textBox1.Tag));
                textBox1.Text = Convert.ToString(pls);
                sq = false;
            }

              
        }
    }
}
